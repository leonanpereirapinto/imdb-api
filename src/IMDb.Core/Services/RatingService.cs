﻿using System;
using System.Linq;
using System.Threading.Tasks;
using IMDb.Core.Interfaces;

namespace IMDb.Core.Services
{
    public class RatingService : IRatingService
    {
        private readonly IMovieRepository _movieRepository;

        public RatingService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public async Task<double?> GetMovieAverageRating(Guid movieId)
        {
            var reviews = await _movieRepository.GetReviewsByMovieId(movieId);

            if (reviews.Any())
            {
                return reviews.Average(r => r.Rating);
            }

            return null;
        }
    }
}