﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IMDb.Core.Models;
using IMDb.SharedKernel.Data;

namespace IMDb.Core.Interfaces
{
    public interface IMovieRepository : IRepository<Movie>
    {
        Task<PagedResult<Movie>> GetAll(int? pageSize, int? pageIndex, 
            string director = null, string name = null, string genre = null, string actor = null);

        Task<Movie> GetById(Guid id);
        
        void Add(Movie movie);

        void Update(Movie movie);

        Task<Review> GetReviewByMovieIdAndUserId(Guid movieId, Guid userId);
        
        Task<List<Review>> GetReviewsByMovieId(Guid movieId);

        Task<List<Review>> GetReviewsByUserId(Guid userId);

        Task<Review> GetReviewsById(Guid id);

        void AddReview(Review review);

        void UpdateReview(Review review);
    }
}