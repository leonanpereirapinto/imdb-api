﻿using System.Collections.Generic;
using IMDb.Core.Notifications;

namespace IMDb.Core.Interfaces
{
    public interface INotifier
    {
        bool HasNotification();
        List<Notification> GetNotifications();
        void Handle(Notification notification);
    }
}