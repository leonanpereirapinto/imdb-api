﻿using System;
using System.Threading.Tasks;

namespace IMDb.Core.Interfaces
{
    public interface IRatingService
    {
        Task<double?> GetMovieAverageRating(Guid movieId);
    }
}