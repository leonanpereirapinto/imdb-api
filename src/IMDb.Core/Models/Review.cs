﻿using System;
using IMDb.SharedKernel;
using IMDb.SharedKernel.DomainObjects;

namespace IMDb.Core.Models
{
    public class Review : Entity
    {
        public Guid UserId { get; private set; }
        public Guid MovieId { get; private set; }
        public string Title { get; private set; }
        public byte Rating { get; private set; }
        public string Comment { get; private set; }

        public User User { get; private set; }
        public Movie Movie { get; private set; }

        protected Review() { }

        public Review(string title, byte rating, string comment, User user, Movie movie)
        {
            Title = title;
            Rating = rating;
            Comment = comment;

            UserId = user.Id;
            User = user;

            MovieId = movie.Id;
            Movie = movie;

            Validate();
        }

        public void ChangeUser(User user)
        {
            Validations.ValidateIfTrue(user.Admin, "Admin user cannot create reviews");

            UserId = user.Id;
            User = user;
        }

        public void ChangeMovie(Movie movie)
        {
            MovieId = movie.Id;
            Movie = movie;
        }

        public void ChangeTitle(string title)
        {
            Validations.ValidateIfEmpty(title, "The Title field cannot be empty");

            Title = title;
        }

        public void ChangeRating(byte rating)
        {
            Validations.ValidateMinMax(rating, 0, 4, "The Rating field must be between 0 and 4");

            Rating = rating;
        }

        public void ChangeComment(string comment)
        {
            Comment = comment;
        }

        public void Validate()
        {
            Validations.ValidateIfEmpty(Title, "The Title field cannot be empty");
            Validations.ValidateMinMax(Rating, 0, 4, "The Rating field must be between 0 and 4");
            Validations.ValidateIfTrue(User.Admin, "Admin user cannot create reviews");
        }
    }
}