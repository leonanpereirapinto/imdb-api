﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using IMDb.SharedKernel;
using IMDb.SharedKernel.DomainObjects;
using Microsoft.AspNetCore.Identity;

namespace IMDb.Core.Models
{
    public class User : IdentityUser<Guid>, IEntity, IAggregateRoot
    {
        private string _email;
        
        [ProtectedPersonalData]
        public override string UserName => Email;
        [ProtectedPersonalData]
        public override string Email => _email;

        [ProtectedPersonalData]
        public string Name { get; private set; }
        
        public bool Admin { get; private set; }

        public bool Active { get; private set; }

        public ICollection<Review> Reviews { get; private set; }

        protected User() { }

        public User(string name, string email, bool admin, bool active)
        {
            Id = Guid.NewGuid();
            Name = name;
            _email = email;
            Admin = admin;
            Active = active;

            EmailConfirmed = true;

            Validate();
        }

        public void ChangeName(string name)
        {
            Validations.ValidateIfEmpty(name, "The Name field cannot be empty");
            Validations.ValidateLength(name, 1, 250, "The Name field must be between 1 and 250 characters");

            Name = name;
        }

        public void ChangeEmail(string email)
        {
            ValidateEmail(email);

            _email = email;
        }

        public void Validate()
        {
            Validations.ValidateIfEmpty(Name, "The Name field cannot be empty");
            Validations.ValidateLength(Name, 1, 250, "The Name field must be between 1 and 250 characters");

            ValidateEmail(_email);
        }

        public void Activate()
        {
            Active = true;
        }

        public void Deactivate()
        {
            Active = false;
        }
        
        private void ValidateEmail(string email)
        {
            Validations.ValidateIfEmpty(email, "The Email field cannot be empty");

            var regexEmail = new Regex(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");

            Validations.ValidateByPattern(regexEmail, email, "Invalid e-mail");
        }
    }
}