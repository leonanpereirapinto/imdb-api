﻿using System.Collections.Generic;
using IMDb.SharedKernel;
using IMDb.SharedKernel.DomainObjects;

namespace IMDb.Core.Models
{
    public class Movie : Entity, IAggregateRoot
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        
        public string Director { get; private set; }
        public string Genre { get; private set; }
        public string Actors { get; private set; }

        public ICollection<Review> Reviews { get; private set; }

        protected Movie() { }

        public Movie(string name, string description, string director, string genre, string actors)
        {
            Name = name;
            Description = description;
            Director = director;
            Genre = genre;
            Actors = actors;

            Validate();
        }

        public void ChangeName(string name)
        {
            Validations.ValidateIfEmpty(name, "The Name field cannot be empty");
            Validations.ValidateLength(name, 1, 250, "The Name field must be between 1 and 250 characters");
            
            Name = name;
        }

        public void ChangeDescription(string description)
        {
            Validations.ValidateIfEmpty(description, "The Description field cannot be empty");
            Validations.ValidateLength(description, 1, 1000, "The Title field must be between 1 and 1000 characters");

            Description = description;
        }

        public void Validate()
        {
            Validations.ValidateIfEmpty(Name, "The Name field cannot be empty");
            Validations.ValidateLength(Name, 1, 250, "The Name field must be between 1 and 250 characters");
            Validations.ValidateIfEmpty(Description, "The Description field cannot be empty");
            Validations.ValidateLength(Description, 1, 1000, "The Description field must be between 1 and 1000 characters");
        }
    }
}