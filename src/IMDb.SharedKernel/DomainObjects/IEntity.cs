﻿using System;

namespace IMDb.SharedKernel.DomainObjects
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}