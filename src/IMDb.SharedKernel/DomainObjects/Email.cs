﻿using System.Text.RegularExpressions;

namespace IMDb.SharedKernel.DomainObjects
{
    public class Email
    {
        public string EmailAddress { get; private set; }

        public Email(string emailAddress)
        {
            EmailAddress = emailAddress;

            Validate();
        }

        public void Validate()
        {
            Validations.ValidateIfEmpty(EmailAddress, "The EmailAddress field cannot be empty");

            var regexEmail = new Regex(@"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$");

            Validations.ValidateByPattern(regexEmail, EmailAddress, "Invalid e-mail");
        }
    }
}