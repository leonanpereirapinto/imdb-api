﻿using System;
using IMDb.SharedKernel.DomainObjects;

namespace IMDb.SharedKernel.Data
{
    public interface IRepository<T> : IDisposable where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}