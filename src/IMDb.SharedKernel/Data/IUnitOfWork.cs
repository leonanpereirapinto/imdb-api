﻿using System.Threading.Tasks;

namespace IMDb.SharedKernel.Data
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}