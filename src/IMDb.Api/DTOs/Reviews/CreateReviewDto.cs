﻿using System;
using System.ComponentModel.DataAnnotations;
using AutoMapper.Configuration.Annotations;
using IMDb.Api.Classes;

namespace IMDb.Api.DTOs.Reviews
{
    public class CreateReviewDto
    {
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        public Guid MovieId { get; set; }

        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        public string Title { get; set; }
        
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [Range(0, 4)]
        public byte Rating { get; set; }
        
        public string Comment { get; set; }
    }
}