﻿using System;
using System.ComponentModel.DataAnnotations;
using IMDb.Api.Classes;

namespace IMDb.Api.DTOs.Movies
{
    public class UpdateMovieDto
    {
        public Guid Id { get; set; }
        
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [StringLength(250, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Name { get; private set; }
        
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [StringLength(1000, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Description { get; private set; }
        
        public string Director { get; private set; }
        public string Genre { get; private set; }
        public string Actors { get; private set; }
    }
}