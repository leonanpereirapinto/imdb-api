﻿using System.ComponentModel.DataAnnotations;
using IMDb.Api.Classes;

namespace IMDb.Api.DTOs.Movies
{
    public class CreateMovieDto
    {
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [StringLength(250, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Name { get; set; }
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [StringLength(1000, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Description { get; set; }
        
        public string Director { get; set; }
        public string Genre { get; set; }
        public string Actors { get; set; }
    }
}