﻿using System;

namespace IMDb.Api.DTOs.Movies
{
    public class MovieDetailDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public string Director { get; set; }
        public string Genre { get; set; }
        public string Actors { get; set; }
        public double? Average { get; set; }
    }
}