﻿using System.ComponentModel.DataAnnotations;
using IMDb.Api.Classes;

namespace IMDb.Api.DTOs.Users
{
    public class LoginUserDto
    {
        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [EmailAddress(ErrorMessage = Constants.WrongFormatValidationMessage)]
        public string Email { get; set; }

        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [StringLength(100, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Password { get; set; }
    }
}