﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using IMDb.Api.Classes;

namespace IMDb.Api.DTOs.Users
{
    public class UpdateUserDto
    {
        [StringLength(250, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Name { get; set; }
        
        [StringLength(100, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 6)]
        public string CurrentPassword { get; set; }
        
        [StringLength(100, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 6)]
        public string NewPassword { get; set; }

        [NotNull]
        [StringLength(100, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        [EmailAddress(ErrorMessage = Constants.WrongFormatValidationMessage)]
        public string Email { get; set; }
    }
}