﻿using System;

namespace IMDb.Api.DTOs.Users
{
    public class UserDto
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }

        public string Email { get; set; }
        
        public bool Admin { get; set; }

        public bool Active { get; set; }
    }
}