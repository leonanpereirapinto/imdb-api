﻿using System.ComponentModel.DataAnnotations;
using IMDb.Api.Classes;

namespace IMDb.Api.DTOs.Users
{
    public class CreateUserDto
    {
        [StringLength(250, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        public string Name { get; set; }
        
        [StringLength(100, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = Constants.RequiredFieldValidationMessage)]
        [StringLength(250, ErrorMessage = Constants.StringLengthValidationMessage, MinimumLength = 1)]
        [EmailAddress(ErrorMessage = Constants.WrongFormatValidationMessage)]
        public string Email { get; set; }
    }
}