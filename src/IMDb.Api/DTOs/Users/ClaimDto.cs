﻿namespace IMDb.Api.DTOs.Users
{
    public class ClaimDto
    {
        public string Value { get; set; }
        public string Type { get; set; }
    }
}