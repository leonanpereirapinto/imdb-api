﻿using System.Threading.Tasks;
using IMDb.Api.Controllers.Base;
using IMDb.Api.DTOs.Reviews;
using IMDb.Api.Interfaces;
using IMDb.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Api.Controllers
{
    [Authorize]
    [Route("movies")]
    public class ReviewsController : BaseController
    {
        private readonly IReviewsAppService _reviewsAppService;

        public ReviewsController(INotifier notifier, IAppUser appUser, IReviewsAppService reviewsAppService) : base(notifier, appUser)
        {
            _reviewsAppService = reviewsAppService;
        }
        
        [Authorize]
        [HttpPost("vote")]
        public async Task<ActionResult> Create(CreateReviewDto createReviewDto)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);
            
            await _reviewsAppService.Create(AppUser.GetUserId(), createReviewDto);
            
            return CustomResponse();
        }
    }
}