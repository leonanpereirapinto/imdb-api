﻿using System;
using System.Threading.Tasks;
using IMDb.Api.Attributes;
using IMDb.Api.Controllers.Base;
using IMDb.Api.DTOs.Movies;
using IMDb.Api.Interfaces;
using IMDb.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Api.Controllers
{
    [Route("movies")]
    public class MoviesController : BaseController
    {
        private readonly IMoviesAppService _moviesAppService;

        public MoviesController(INotifier notifier, IAppUser appUser, IMoviesAppService moviesAppService) : base(notifier, appUser)
        {
            _moviesAppService = moviesAppService;
        }
        
        [HttpGet]
        public async Task<ActionResult> Index([FromQuery] int? pageSize = null, [FromQuery] int? page = null,
            [FromQuery] string director = null, [FromQuery] string name = null, [FromQuery] string genre = null,
            [FromQuery] string actor = null)
        {
            return CustomResponse(await _moviesAppService.GetAll(pageSize, page, director, name, genre, actor));
        }
        
        [HttpGet("{id:Guid}")]
        public async Task<ActionResult> Show(Guid id)
        {
            return CustomResponse(await _moviesAppService.GetDetailsById(id));
        }

        [Authorize]
        [ClaimsAuthorize("Admin", "true")]
        [HttpPost]
        public async Task<ActionResult> Create(CreateMovieDto createMoviesDto)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            await _moviesAppService.Create(createMoviesDto);
            
            return CustomResponse();
        }
    }
}