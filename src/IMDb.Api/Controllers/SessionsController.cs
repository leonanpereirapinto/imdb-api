﻿using System.Threading.Tasks;
using IMDb.Api.Controllers.Base;
using IMDb.Api.DTOs.Users;
using IMDb.Api.Interfaces;
using IMDb.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Api.Controllers
{
    [Route("sessions")]
    public class SessionsController : BaseController
    {
        private readonly IUserAppService _userAppService;

        public SessionsController(INotifier notifier, IAppUser appUser, IUserAppService userAppService) : base(notifier, appUser)
        {
            _userAppService = userAppService;
        }

        [HttpPost]
        public async Task<ActionResult> Create(LoginUserDto loginUser)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            return CustomResponse(await _userAppService.GenerateJwt(loginUser));
        }
    }
}