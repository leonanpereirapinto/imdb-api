﻿using System;
using System.Threading.Tasks;
using IMDb.Api.Attributes;
using IMDb.Api.Controllers.Base;
using IMDb.Api.DTOs.Users;
using IMDb.Api.Interfaces;
using IMDb.Core.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace IMDb.Api.Controllers
{
    [Route("users")]
    public class UserController : BaseController
    {
        private readonly IUserAppService _userAppService;

        public UserController(INotifier notifier, IAppUser appUser, IUserAppService userAppService) : base(notifier, appUser)
        {
            _userAppService = userAppService;
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateUserDto createUser)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            await _userAppService.Add(createUser, false);

            return CustomResponse();
        }

        [HttpPut]
        [Authorize]
        public async Task<ActionResult> Update(UpdateUserDto updateUser)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);
            
            await _userAppService.Update(AppUser.GetUserId(), updateUser);

            return CustomResponse();
        }
        
        [HttpDelete]
        [Authorize]
        public async Task<ActionResult> Delete()
        {
            await _userAppService.Deactivate(AppUser.GetUserId());

            return CustomResponse();
        }
    }
}