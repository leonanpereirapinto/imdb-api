﻿using System;
using System.Linq;
using IMDb.Core.Interfaces;
using IMDb.Core.Notifications;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace IMDb.Api.Controllers.Base
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        private readonly INotifier _notifier;

        public IAppUser AppUser { get; }
        
        public bool AuthenticatedUser { get; }

        public Guid UserId { get; }
        
        public BaseController(INotifier notifier, IAppUser appUser)
        {
            _notifier = notifier;
            
            AppUser = appUser;
            
            if (appUser.IsAuthenticated())
            {
                UserId = appUser.GetUserId();
                AuthenticatedUser = true;
            }
        }

        protected bool IsValidOperation()
        {
            return !_notifier.HasNotification();
        }
        
        protected ActionResult CustomResponse(object result = null)
        {
            return CustomResponse<object>(result);
        }

        protected ActionResult CustomResponse<TEntity>(TEntity result)
        {
            if (IsValidOperation())
            {
                return Ok(new
                {
                    success = true, data = result
                });
            }

            return BadRequest(new
            {
                success = false, errors = _notifier.GetNotifications().Select(n => n.Message)
            });
        }

        protected ActionResult CustomResponse(ModelStateDictionary modelState)
        {
            if(!modelState.IsValid) NotifyInvalidModelState(modelState);
            
            return CustomResponse();
        }

        protected void NotifyInvalidModelState(ModelStateDictionary modelState)
        {
            foreach (var error in modelState.Values.SelectMany(e => e.Errors)) 
                NotifyError(error.Exception == null ? error.ErrorMessage : error.Exception.Message);
        }

        protected void NotifyError(string message)
        {
            _notifier.Handle(new Notification(message));
        }
    }
}