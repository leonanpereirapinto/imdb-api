﻿using System;
using System.Threading.Tasks;
using IMDb.Api.DTOs.Movies;
using IMDb.SharedKernel.Data;

namespace IMDb.Api.Interfaces
{
    public interface IMoviesAppService
    {
        Task<PagedResult<MovieDto>> GetAll(int? pageSize, int? page, string director = null, string name = null, string genre = null, string actor = null);

        Task<MovieDetailDto> GetDetailsById(Guid id);

        Task Create(CreateMovieDto createMoviesDto);
        
        Task Update(Guid id, UpdateMovieDto updateMovieDto);
    }
}