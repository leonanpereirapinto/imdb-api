﻿using System;
using System.Threading.Tasks;
using IMDb.Api.DTOs.Reviews;

namespace IMDb.Api.Interfaces
{
    public interface IReviewsAppService
    {
        Task Create(Guid userId, CreateReviewDto createReviewDto1);
    }
}