﻿using System;
using System.Threading.Tasks;
using IMDb.Api.DTOs.Users;
using IMDb.Core.Models;
using IMDb.SharedKernel.Data;

namespace IMDb.Api.Interfaces
{
    public interface IUserAppService
    {
        Task<PagedResult<UserDto>> GetAll(int? pageSize = null, int? pageIndex = null, bool? admin = null);
        Task<UserDto> GetById(Guid id);
        Task Add(CreateUserDto user, bool admin);
        Task Update(Guid id, UpdateUserDto updateUser);

        Task Deactivate(Guid id);

        Task<LoginResponseDto> GenerateJwt(LoginUserDto loginUser);
    }
}