﻿using AutoMapper;
using IMDb.Api.AutoMapper;
using IMDb.Api.Classes;
using IMDb.Api.Interfaces;
using IMDb.Api.Services;
using IMDb.Core.Interfaces;
using IMDb.Core.Notifications;
using IMDb.Core.Services;
using IMDb.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace IMDb.Api.Setup
{
    public static class DependencyInjection
    {
        public static void ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<INotifier, Notifier>();
            services.AddScoped<IAppUser, AppUser>();
            
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();
            
            // Application Services
            services.AddScoped<IMoviesAppService, MoviesAppAppService>();
            services.AddScoped<IUserAppService, UserAppService>();
            services.AddScoped<IReviewsAppService, ReviewsAppService>();
            services.AddScoped<IReviewsAppService, ReviewsAppService>();

            // Repository
            services.AddScoped<IMovieRepository, MovieRepository>();
            
            // Services
            services.AddScoped<IRatingService, RatingService>();
            
            services.AddAutoMapper(typeof(DtoToDomainMappingProfile), typeof(DomainToDtoMappingProfile));
        }
    }
}