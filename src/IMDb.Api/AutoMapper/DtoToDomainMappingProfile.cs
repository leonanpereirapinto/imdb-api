﻿using AutoMapper;
using IMDb.Api.DTOs.Movies;
using IMDb.Api.DTOs.Users;
using IMDb.Core.Models;

namespace IMDb.Api.AutoMapper
{
    public class DtoToDomainMappingProfile : Profile
    {
        public DtoToDomainMappingProfile()
        {
            CreateMap<CreateMovieDto, Movie>()
                .ConstructUsing(m => new Movie(
                    m.Name, m.Description, m.Director, m.Genre, m.Actors));
            
            CreateMap<UpdateMovieDto, Movie>()
                .ConstructUsing(m => new Movie(
                    m.Name, m.Description, m.Director, m.Genre, m.Actors));

            CreateMap<UserDto, User>()
                .ConstructUsing(u => new User(u.Name, u.Email, u.Admin, u.Active));
        }
    }
}