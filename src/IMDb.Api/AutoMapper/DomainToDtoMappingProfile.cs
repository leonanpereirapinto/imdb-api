﻿using AutoMapper;
using IMDb.Api.DTOs.Movies;
using IMDb.Api.DTOs.Users;
using IMDb.Core.Models;

namespace IMDb.Api.AutoMapper
{
    public class DomainToDtoMappingProfile : Profile
    {
        public DomainToDtoMappingProfile()
        {
            CreateMap<Movie, CreateMovieDto>();
            CreateMap<Movie, UpdateMovieDto>();
            CreateMap<Movie, MovieDto>();
            CreateMap<Movie, MovieDetailDto>();

            CreateMap<User, UserDto>();
            CreateMap<User, CreateUserDto>();
            CreateMap<User, UpdateUserDto>();
        }
    }
}