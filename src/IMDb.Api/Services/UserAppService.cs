﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using IMDb.Api.Classes;
using IMDb.Api.DTOs.Users;
using IMDb.Api.Interfaces;
using IMDb.Api.Services.Base;
using IMDb.Core.Interfaces;
using IMDb.Core.Models;
using IMDb.SharedKernel.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace IMDb.Api.Services
{
    public class UserAppService : BaseAppService, IUserAppService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        
        private readonly AppSettings _appSettings;
        private readonly IMapper _mapper;

        public UserAppService(INotifier notifier, UserManager<User> userManager, SignInManager<User> signInManager, IOptions<AppSettings> appSettings, IMapper mapper) : base(notifier)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _appSettings = appSettings.Value;
            _mapper = mapper;
        }

        public async Task<PagedResult<UserDto>> GetAll(int? pageSize = null, int? pageIndex = null, bool? admin = null)
        {
            IQueryable<User> query = _userManager.Users.OrderBy(m => m.Name).Where(u => u.Active);

            if (admin.HasValue)
            {
                query = query.Where(u => u.Admin == admin.Value);
            }
            
            var totalResults = await query.CountAsync();

            if (pageSize.HasValue && pageIndex.HasValue)
            {
                query = query.Skip(pageSize.Value * (pageIndex.Value - 1)).Take(pageSize.Value);
            }

            var users = _mapper.Map<List<UserDto>>(await query.ToListAsync());
            
            return new PagedResult<UserDto>
            {
                List = users,
                TotalResults = totalResults,
                PageIndex = pageIndex,
                PageSize = pageSize
            };
        }

        public async Task<UserDto> GetById(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            
            if (user == null)
            {
                Notify("User not found");
                return null;
            }
            
            return _mapper.Map<User, UserDto>(user);
        }

        public async Task Add(CreateUserDto createUser, bool admin)
        {
            var user = new User(createUser.Name, createUser.Email, admin, true);
            
            var result = await _userManager.CreateAsync(user, createUser.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors.Select(e => e.Description))
                {
                    Notify(error);
                }
            }
            
            var createdUser = await _userManager.FindByEmailAsync(createUser.Email);

            if (createdUser == null)
            {
                Notify("Error when getting created user");
                return;
            }
            
            if (admin)
            {
                var adminClaim = await _userManager.AddClaimAsync(createdUser, new Claim(Constants.AdminClaimName, "true"));
                
                if(!adminClaim.Succeeded) foreach (var error in adminClaim.Errors) Notify(error.Description);
            }
        }

        public async Task Update(Guid id, UpdateUserDto updateUser)
        {
            
            var existingUser =  await _userManager.FindByIdAsync(id.ToString());
            
            if (existingUser == null)
            {
                Notify("User not found");
                return;
            }

            if (!string.IsNullOrEmpty(updateUser.Email))
            {
                var existingUserEmail =  await _userManager.FindByEmailAsync(updateUser.Email);

                if (existingUserEmail != null && existingUserEmail.Id != existingUser.Id)
                {
                    Notify("E-mail is already used");
                    return;
                }
                
                existingUser.ChangeEmail(updateUser.Email);
            }

            if (!string.IsNullOrEmpty(updateUser.NewPassword))
            {
                var changePassword = await _userManager.ChangePasswordAsync(existingUser, updateUser.CurrentPassword, updateUser.NewPassword);

                if (!changePassword.Succeeded)
                {
                    foreach (var error in changePassword.Errors.Select(e => e.Description))
                    {
                        Notify(error);
                    }
                    
                    return;
                }
            }
            
            if (!string.IsNullOrEmpty(updateUser.Name) && updateUser.Name != existingUser.Name)
            {
                existingUser.ChangeName(updateUser.Name);
            }
            
            var result = await _userManager.UpdateAsync(existingUser);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors.Select(e => e.Description))
                {
                    Notify(error);
                }
            }
        }

        public async Task Deactivate(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());

            if (user == null)
            {
                Notify("User not found");
                return;
            }
            
            user.Deactivate();

            await _userManager.UpdateAsync(user);
        }

        public async Task<LoginResponseDto> GenerateJwt(LoginUserDto loginUser)
        {
            var result = await _signInManager.PasswordSignInAsync(loginUser.Email, loginUser.Password, false, true);
            
            if (result.Succeeded)
            {
                return await JwtUtils.GenerateJwt(_userManager, _appSettings, loginUser.Email);
            }
            
            if (result.IsLockedOut)
            {
                Notify("User temporarily blocked by invalid tentatives");
                return null;
            }

            Notify("Wrong email or password");
            return null;
        }
    }
}