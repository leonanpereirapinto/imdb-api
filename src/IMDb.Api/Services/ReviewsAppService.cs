﻿using System;
using System.Threading.Tasks;
using IMDb.Api.DTOs.Reviews;
using IMDb.Api.Interfaces;
using IMDb.Api.Services.Base;
using IMDb.Core.Interfaces;
using IMDb.Core.Models;
using Microsoft.AspNetCore.Identity;

namespace IMDb.Api.Services
{
    public class ReviewsAppService : BaseAppService, IReviewsAppService
    {
        private readonly UserManager<User> _userManager;
        private readonly IMovieRepository _movieRepository;

        public ReviewsAppService(INotifier notifier, UserManager<User> userManager, IMovieRepository movieRepository) : base(notifier)
        {
            _userManager = userManager;
            _movieRepository = movieRepository;
        }

        public async Task Create(Guid userId, CreateReviewDto createReviewDto1)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());

            if (user.Admin)
            {
                Notify("Only users can rate movies");
                return;
            }

            var existingReview = await _movieRepository.GetReviewByMovieIdAndUserId(createReviewDto1.MovieId, userId);

            if (existingReview != null)
            {
                Notify("You have already rated this movie");
                return;
            }

            var existingMovie = await _movieRepository.GetById(createReviewDto1.MovieId);

            if (existingMovie == null)
            {
                Notify("Movie not found");
                return;
            }
            
            var review = new Review(createReviewDto1.Title, createReviewDto1.Rating, createReviewDto1.Comment, user, existingMovie);

            _movieRepository.AddReview(review);

            await _movieRepository.UnitOfWork.Commit();
        }
    }
}