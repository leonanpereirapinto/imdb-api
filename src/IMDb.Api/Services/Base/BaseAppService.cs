﻿using IMDb.Core.Interfaces;
using IMDb.Core.Notifications;

namespace IMDb.Api.Services.Base
{
    public class BaseAppService
    {
        private readonly INotifier _notifier;

        protected BaseAppService(INotifier notifier)
        {
            _notifier = notifier;
        }
        
        protected void Notify(string message) => _notifier.Handle(new Notification(message));
    }
}