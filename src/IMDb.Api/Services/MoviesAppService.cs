﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using IMDb.Api.DTOs.Movies;
using IMDb.Api.Interfaces;
using IMDb.Api.Services.Base;
using IMDb.Core.Interfaces;
using IMDb.Core.Models;
using IMDb.SharedKernel.Data;

namespace IMDb.Api.Services
{
    public class MoviesAppAppService : BaseAppService, IMoviesAppService
    {
        private readonly IMovieRepository _moviesRepository;
        private readonly IMapper _mapper;
        private readonly IRatingService _ratingService;

        public MoviesAppAppService(INotifier notifier, IMapper mapper, IMovieRepository moviesRepository, IRatingService ratingService) : base(notifier)
        {
            _mapper = mapper;
            _moviesRepository = moviesRepository;
            _ratingService = ratingService;
        }

        public async Task<PagedResult<MovieDto>> GetAll(int? pageSize, int? page, string director = null, string name = null, string genre = null, string actor = null)
        {
            var moviePagedResult = await _moviesRepository.GetAll(pageSize, page, director, name, genre, actor);

            var movies = _mapper.Map<List<MovieDto>>(moviePagedResult.List);

            return new PagedResult<MovieDto>
            {
                List = movies,
                PageIndex = moviePagedResult.PageIndex,
                PageSize = moviePagedResult.PageSize,
                TotalResults = moviePagedResult.TotalResults
            };
        }

        public async Task<MovieDetailDto> GetDetailsById(Guid id)
        {
            var movie = await _moviesRepository.GetById(id);
            
            if (movie == null)
            {
                Notify("Movie not found");
                return null;
            }

            var movieDto = _mapper.Map<Movie, MovieDetailDto>(movie);

            movieDto.Average = await _ratingService.GetMovieAverageRating(id);

            return movieDto;
        }

        public async Task Create(CreateMovieDto createMoviesDto)
        {
            var movie = _mapper.Map<CreateMovieDto, Movie>(createMoviesDto);

            _moviesRepository.Add(movie);

            await _moviesRepository.UnitOfWork.Commit();
        }

        public async Task Update(Guid id, UpdateMovieDto updateMovieDto)
        {
            if (id != updateMovieDto.Id)
            {
                Notify("IDs are not the same");
                return;
            }
            
            var movie = _mapper.Map<UpdateMovieDto, Movie>(updateMovieDto);
            
            _moviesRepository.Update(movie);
            
            await _moviesRepository.UnitOfWork.Commit();
        }
    }
}