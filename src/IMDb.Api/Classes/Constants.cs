﻿namespace IMDb.Api.Classes
{
    public static class Constants
    {
        public const string RequiredFieldValidationMessage = "The {0} field is required";
        public const string StringLengthValidationMessage = "The {0} field must be between {2} and {1} characters";
        public const string WrongFormatValidationMessage = "The {0} field has an invalid format";
        public const string AdminClaimName = "Admin";
    }
}