﻿using IMDb.Core.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace IMDb.Data.Mappings
{
    public class ReviewMappings : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasKey(c => c.Id);
            
            builder.Property(c => c.Title)
                .IsRequired()
                .HasColumnType("varchar(250)");
            
            builder.Property(c => c.Comment)
                .HasColumnType("varchar(1000)");

            builder.HasOne(r => r.User)
                .WithMany(u => u.Reviews)
                .HasForeignKey(r => r.UserId);

            builder.ToTable("Reviews");
        }
    }
}