﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IMDb.Core.Interfaces;
using IMDb.Core.Models;
using IMDb.Data.Context;
using IMDb.SharedKernel.Data;
using Microsoft.EntityFrameworkCore;

namespace IMDb.Data.Repositories
{
    public class MovieRepository : IMovieRepository
    {
        private readonly DataDbContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public MovieRepository(DataDbContext context)
        {
            _context = context;
        }
        
        public async Task<PagedResult<Movie>> GetAll(int? pageSize, int? pageIndex, 
            string director = null, string name = null, string genre = null, string actor = null)
        {
            IQueryable<Movie> query = _context.Movies.Include(m => m.Reviews)
                .OrderBy(m => m.Reviews.Count)
                .ThenBy(m => m.Name);
            
            if (!string.IsNullOrEmpty(director) || !string.IsNullOrEmpty(name) || !string.IsNullOrEmpty(genre) || !string.IsNullOrEmpty(actor))
            {
                query = query.Where(m =>
                    m.Director != null && director != null && EF.Functions.Like(m.Director, $"%{director}%") || 
                    m.Name != null && name != null && EF.Functions.Like(m.Name, $"%{name}%") || 
                    m.Genre != null && genre != null && EF.Functions.Like(m.Genre, $"%{genre}%") || 
                    m.Actors != null && actor != null && EF.Functions.Like(m.Actors, $"%{actor}%"));
            }
            
            var totalResults = await query.CountAsync();

            if (pageSize.HasValue && pageIndex.HasValue)
            {
                query = query.Skip(pageSize.Value * (pageIndex.Value - 1)).Take(pageSize.Value);
            }

            var movieList = await query.ToListAsync();

            return new PagedResult<Movie>
            {
                List = movieList,
                TotalResults = totalResults,
                PageIndex = pageIndex,
                PageSize = pageSize
            };
        }

        public async Task<Movie> GetById(Guid id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public void Add(Movie movie)
        {
            _context.Movies.Add(movie);
        }

        public void Update(Movie movie)
        {
            _context.Movies.Update(movie);
        }

        public async Task<List<Review>> GetReviewsByMovieId(Guid movieId)
        {
            return await _context.Reviews.Where(r => r.MovieId == movieId)
                .Include(r => r.User)
                .Include(r => r.Movie).ToListAsync();
        }
        
        public async Task<List<Review>> GetReviewsByUserId(Guid userId)
        {
            return await _context.Reviews.Where(r => r.UserId == userId)
                .Include(r => r.User)
                .Include(r => r.Movie).ToListAsync();
        }
        
        public async Task<Review> GetReviewByMovieIdAndUserId(Guid movieId, Guid userId)
        {
            return await _context.Reviews
                .Include(r => r.User)
                .Include(r => r.Movie)
                .FirstOrDefaultAsync(r => r.MovieId == movieId && r.UserId == userId);
        }

        public async Task<Review> GetReviewsById(Guid id)
        {
            return await _context.Reviews.FindAsync(id);
        }

        public void AddReview(Review review)
        {
            _context.Reviews.Add(review);
        }

        public void UpdateReview(Review review)
        {
            _context.Reviews.Update(review);
        }
        
        public void Dispose()
        {
            _context?.Dispose();
        }
    }
}