# IMDb API

## Instruções para rodar o projeto:

- Banco de dados utilizado: SQL Server
- Na pasta do código fonte, executar as migrations: 

```
dotnet ef database update -s ./src/IMDb.Api -p ./src/IMDb.Data
```

- Para executar a API:

```
dotnet run -p ./src/IMDb.Api
``` 

Feito isso já é possível acessar o Swagger: 
https://localhost:5001/swagger